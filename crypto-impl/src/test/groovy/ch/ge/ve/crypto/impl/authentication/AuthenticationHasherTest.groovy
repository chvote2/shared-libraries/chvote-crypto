/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-crypto                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.crypto.impl.authentication

import ch.ge.ve.crypto.authentication.AuthenticationHasher
import ch.ge.ve.crypto.authentication.HashedAuthentication
import java.security.SecureRandom
import spock.lang.Specification

class AuthenticationHasherTest extends Specification {

  def "hashing an authentication content should always return correct informations"() {
    given:
    def authenticationInformation = randomString(randomInt(1, 200))
    def iterations = randomInt(10000, 11000) //between 10000 to 11000 iterations

    when:
    AuthenticationHasher authenticationHasher = new AuthenticationHasherImpl()
    HashedAuthentication hashedAuthentication = authenticationHasher.hash(authenticationInformation, authenticationHasher.getSalt(), iterations)

    then:
    hashedAuthentication.iterations() == iterations
    hashedAuthentication.salt() != null
    hashedAuthentication.salt().size() == 16
    hashedAuthentication.hash() != null
    hashedAuthentication.hash().size() == 64
  }

  int randomInt(int min, int max) {
    return new Random().nextInt(max - min + 1) + min
  }

  String randomString(int len) {
    String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    SecureRandom rnd = new SecureRandom()
    StringBuilder sb = new StringBuilder(len)
    for (int i = 0; i < len; i++) {
      sb.append(AB.charAt(rnd.nextInt(AB.length())))
    }
    return sb.toString()
  }

  def "the comparison of two hashes should be equal"() {
    given:
    def authenticationInformation = randomString(randomInt(1, 200))
    def iterations = randomInt(10000, 11000) //between 10000 to 11000 iterations

    when:
    AuthenticationHasher authenticationHasher = new AuthenticationHasherImpl()
    HashedAuthentication hashedAuthentication = authenticationHasher.hash(authenticationInformation, authenticationHasher.getSalt(), iterations)
    HashedAuthentication anotherHashedAuthentication = authenticationHasher.hash(authenticationInformation, hashedAuthentication.salt(), hashedAuthentication.iterations())

    then:
    hashedAuthentication.iterations() == iterations
    hashedAuthentication.salt() != null
    hashedAuthentication.salt().size() == 16
    hashedAuthentication.hash() != null
    hashedAuthentication.hash().size() == 64
    hashedAuthentication.hash() == anotherHashedAuthentication.hash()
  }
}