/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-crypto                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.crypto.authentication;


/**
 * Data transfer object with hashed authentication information
 */
public class HashedAuthentication {

  private final int    iterations;
  private final byte[] salt;
  private final byte[] hash;

  public HashedAuthentication(int iterations, byte[] salt, byte[] hash) {
    this.iterations = iterations;
    this.salt = salt;
    this.hash = hash;
  }

  public int iterations() {
    return this.iterations;
  }

  public byte[] salt() {
    return this.salt;
  }

  public byte[] hash() {
    return this.hash;
  }

}
